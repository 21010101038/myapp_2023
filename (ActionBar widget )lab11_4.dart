import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7_to_12/(NavigationDrawer)lab11_3.dart';

class Lab11_4 extends StatelessWidget {
  const Lab11_4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text("Hello Appbar"),
         leading: GestureDetector(
           onTap: () { /* Write listener code here */ },
           child: Icon(
             Icons.menu,  // add custom icons also
           ),
         ),
         actions: <Widget>[
           Padding(
               padding: EdgeInsets.only(right: 20.0),
               child: GestureDetector(
                 onTap: () {},
                 child: Icon(
                   Icons.search,
                   size: 26.0,
                 ),
               )
           ),
           Padding(
               padding: EdgeInsets.only(right: 20.0),
               child: GestureDetector(
                 onTap: () {
                   Navigator.of(context).push(MaterialPageRoute(builder: (context){
                      return  Lab11_3();
                   },
                   ),
                   );

                 },
                 child: Icon(
                     Icons.more_vert
                 ),
               )
           ),
         ],
       ),
     );
  }
}

