import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab11_1 extends StatelessWidget {
  const Lab11_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      DefaultTabController(
          length: 10,
          child: Scaffold(
            appBar: AppBar(
              elevation: 101,
              title: Center(
                child: Text('Tab_view_Exampal',style: TextStyle(fontSize: 20,backgroundColor: Colors.blue,color: Colors.white,fontWeight: FontWeight.bold),
                ),
              ),
              bottom: TabBar(
                isScrollable:true,
                unselectedLabelColor:Colors.grey,
                mouseCursor: MaterialStateMouseCursor.textable,
                tabs: [
                  Tab(
                    icon: Icon(Icons.home_work),
                    text: "Home",
                  ),
                  Tab(
                    icon: Icon(Icons.save),
                    text: "save",
                  ),
                  Tab(
                    icon: Icon(Icons.account_balance),
                    text: "account_balance",
                  ),
                  Tab(
                    icon: Icon(Icons.back_hand),
                    text: "back_hand",
                  ),
                  Tab(
                    icon: Icon(Icons.wallet_giftcard),
                    text: "wallet_giftcard",
                  ),
                  Tab(
                    icon: Icon(Icons.queue),
                    text: "queue",
                  ),
                  Tab(
                    icon: Icon(Icons.tablet_mac),
                    text: "tablet_mac",
                  ),
                  Tab(
                    icon: Icon(Icons.vape_free_outlined),
                    text: "vape_free_outlined",
                  ),
                  Tab(
                    icon: Icon(Icons.dangerous),
                    text: "dangerous",
                  ),
                  Tab(
                    icon: Icon(Icons.shop_2_rounded),
                    text: "shop_2_rounded",
                  ),
                ],
              ),
            ),
            body: TabBarView(
              children: [
                Center(
                    child: Icon(Icons.home)),
                Center(
                  child: Icon(Icons.save),
                ),
                Center(
                  child: Icon(Icons.cabin),
                ),
                Center(
                  child: Icon(Icons.back_hand),
                ),
                Center(
                  child: Icon(Icons.wallet_giftcard),
                ),
                Center(
                  child: Icon(Icons.add),
                ),
                Center(
                  child: Icon(Icons.tablet_mac),
                ),
                Center(
                  child: Icon(Icons.vape_free_outlined),
                ),
                Center(
                    child: Icon(Icons.dangerous)),
                Center(
                  child: Icon(Icons.shop_2_rounded),
                ),

              ],
            ),
          ),
      );
  }
}
