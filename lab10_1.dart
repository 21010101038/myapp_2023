import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab10_1 extends StatelessWidget {
  const Lab10_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
           borderRadius: BorderRadius.circular(100),
         ),
        margin: EdgeInsets.symmetric(horizontal: 20,),
        child: BottomNavigationBar(
          elevation:100,
          selectedItemColor: Colors.blue,
          unselectedItemColor: Colors.redAccent,
          items: [
                 BottomNavigationBarItem(icon: Icon(Icons.home),label: "Home"),
                 BottomNavigationBarItem(icon: Icon(Icons.search_rounded),label: "search_rounded"),
                 BottomNavigationBarItem(icon: Icon(Icons.diamond),label: "diamond"),
                 BottomNavigationBarItem(icon: Icon(Icons.manage_accounts),label: "manage_accounts"),
               ],
        ),
      ),
    );
  }
}
