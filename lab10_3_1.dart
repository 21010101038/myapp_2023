import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab10_3_1 extends StatelessWidget {
  const Lab10_3_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: (){
               Navigator.pushNamed(context, '/login_2');
            },
            child: Center(
              child: Text(
                "Login_1",
                style: TextStyle(
                  color: Colors.redAccent,
                  backgroundColor: Colors.limeAccent,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
