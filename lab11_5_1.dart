import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7_to_12/Lab11_5_2.dart';

class Lab11_5_1 extends StatefulWidget {



  @override
  State<Lab11_5_1> createState() => _Lab11_5_1State();
}

class _Lab11_5_1State extends State<Lab11_5_1> {
  var formkey = GlobalKey<FormState>();

  bool passenable = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Center(
          child: Form(
            key: formkey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.apple_rounded,
                  size: 150,
                ),
                SizedBox(
                  height: 45,
                ),
                Text(
                  "Hello Again!",
                  style:
                  TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 36,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Welcome back, you\'ve been missed!",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Name';
                          }
                          if (value.length < 20) {
                            return 'Enter Valid Name';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none, hintText: 'Name'),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Enrollment No.';
                          }
                          if (value.length < 11) {
                            return 'Enter Valid Enrollment No.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none, hintText: 'Enrollment No'),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                // email  textfild
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Email';
                          }
                          if (value.length < 20) {
                            return 'Enter Valid Email';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            border: InputBorder.none, hintText: 'Email'),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                // password textfild
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Password';
                          }

                          if (value.length < 8) {
                            return 'Enter Valid password';
                          }

                        },
                        obscureText: passenable,
                        decoration: InputDecoration(
                            border: InputBorder.none, hintText: 'Password',
                          suffix: IconButton(onPressed: (){ //add Icon button at end of TextField
                            setState(() { //refresh UI
                              if(passenable){ //if passenable == true, make it false
                                passenable = false;
                              }else{
                                passenable = true; //if passenable == false, make it true
                              }
                            });
                          }, icon: Icon(passenable==true?Icons.remove_red_eye:Icons.password)))
                        ),
                      ),
                    ),
                  ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: InkWell(
                    onTap: () {
                      if (formkey.currentState!.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) {
                            return Lab11_5_2();
                          },
                        ));
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                ],
            ),





            ),
          ),
        ),
      );

  }
}
