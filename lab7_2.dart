import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab7_2 extends StatelessWidget {
  const Lab7_2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              color: Colors.redAccent,
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.blueGrey,
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.cyan,
            ),
          ),


        ],
      ),
    );
  }
}
