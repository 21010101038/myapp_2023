import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab7_3 extends StatelessWidget {
  const Lab7_3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(child: Container(color: Colors.redAccent,)),
                Expanded(child: Container(color: Colors.deepPurple,)),
                Expanded(child: Container(color: Colors.amberAccent,)),

              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(child: Container(color: Colors.deepOrange,)),
                Expanded(child: Container(color: Colors.pinkAccent,)),
                Expanded(child: Container(color: Colors.black,)),

              ],
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(child: Container(color: Colors.redAccent,)),
                Expanded(child: Container(color: Colors.indigo,)),
                Expanded(child: Container(color: Colors.tealAccent,)),

              ],
            ),
          ),



        ],
      ),



    );
  }
}
