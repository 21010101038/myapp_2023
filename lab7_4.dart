import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab_4 extends StatelessWidget {
  const Lab_4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Expanded(child: Container(color: Colors.redAccent,)),
                Expanded(child: Container(color: Colors.deepPurple,)),
                Expanded(child: Container(color: Colors.amberAccent,)),

              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(child: Container(color: Colors.deepOrange,),flex: 2),
                Expanded(child: Container(color: Colors.pinkAccent,),flex: 2),
                Expanded(child: Container(color: Colors.black,)),

              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(child: Container(color: Colors.redAccent,)),
                Expanded(child: Container(color: Colors.indigo,),flex: 3),
                Expanded(child: Container(color: Colors.tealAccent,),flex: 2),

              ],
            ),
          ),
        ],
      ),
    );
  }
}
