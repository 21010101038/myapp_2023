import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab8_1 extends StatelessWidget {
  const Lab8_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height:300,
          ),
          Center(
            child: Text(
              "hello World",
              style: TextStyle(
                color: Colors.cyan,
                fontSize: 25,
                backgroundColor: Colors.deepOrange,
              ),
            ),
          )
        ],
      ),
    );
  }
}
