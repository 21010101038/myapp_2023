import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab8_2 extends StatelessWidget {
  const Lab8_2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height:300,
          ),
          Center(
            child: Text(
              "hello World",
              style: TextStyle(
                fontSize: 25,
                fontFamily:'Roboto-Medium',
                backgroundColor: Colors.deepOrange,
              ),
            ),
          )
        ],
      ),
    );
  }
}
