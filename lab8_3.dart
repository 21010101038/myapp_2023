import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lab_7_to_12/lab8_2.dart';

class Lab8_3 extends StatefulWidget {
  const Lab8_3({Key? key}) : super(key: key);

  @override
  State<Lab8_3> createState() => _Lab8_3State();
}

class _Lab8_3State extends State<Lab8_3> {
  var formkey=GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child:Center(
          child:Form(
            key: formkey,
            child: Column(

              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.white,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Center(
                        child: TextFormField(
                            onChanged: (value) {
                              print("The value entered is : $value");
                            },
                          style: TextStyle(color: Colors.black, fontSize: 15),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Enter Name ";
                            }
                            if (value.length < 20) {
                              return "Enter Valid Name.";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Name",
                            // label: Column(
                            //     children: [Text("Enter RollNo")]),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.white,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Center(
                        child: TextFormField(
                            onChanged: (value) {
                              print("The value entered is : $value");
                            },
                            style: TextStyle(color: Colors.black, fontSize: 15),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Enter Passworld ";
                              }
                              if (value.length < 20) {
                                return "Enter Valid Passworld.";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Passworld",
                              // label: Column(
                              //     children: [Text("Enter RollNo")]),
                            ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: InkWell(

                    onTap: () {
                      if(formkey.currentState!.validate()){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context){
                          return Lab8_2();
                        },
                        ),
                        );
                      }

                    },
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.deepPurple,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                          child: Text(
                            "Sign in",
                            style: TextStyle(color: Colors.white, fontSize: 20,fontWeight: FontWeight.bold,),
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),

        ),
      ),

    );
  }
}
