import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lab_7_to_12/lab8_2.dart';

class Lab8_4 extends StatefulWidget {
  const Lab8_4({Key? key}) : super(key: key);

  @override
  State<Lab8_4> createState() => _Lab8_4State();
}

class _Lab8_4State extends State<Lab8_4> {
  var formkey=GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.white,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Center(
                  child: TextFormField(
                    onChanged: (value) {
                      print("The value entered is : $value");
                    },
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter Name ";
                      }
                      if (value.length < 20) {
                        return "Enter Valid Name.";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Name",
                      // label: Column(
                      //     children: [Text("Enter RollNo")]),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                  color: Colors.white,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Center(
                  child: TextFormField(
                    onChanged: (value) {
                      print("The value entered is : $value");
                    },
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter Passworld ";
                      }
                      if (value.length < 20) {
                        return "Enter Valid Passworld.";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Passworld",
                      // label: Column(
                      //     children: [Text("Enter RollNo")]),
                    ),
                  ),
                ),
              ),
            ),
          ),

        ],
      ),

    );
  }
}
