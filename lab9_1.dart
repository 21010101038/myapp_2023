import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab9_1 extends StatelessWidget {
  const Lab9_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              child: Image.asset('images/bmw.jpg'),
            ),
          ),
          SizedBox(
            height: 120,
          ),
          Center(
            child: Container(
              child: Image.network("https://image.shutterstock.com/image-photo/adiyogi-shiva-statue-coimbatore-tamil-260nw-1291355458.jpg"),
            ),
          ),
        ],
      ),
    );
  }
}
