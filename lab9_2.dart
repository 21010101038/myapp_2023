import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7_to_12/main.dart';

class Lab9_2 extends StatefulWidget {
  const Lab9_2({Key? key}) : super(key: key);

  @override
  State<Lab9_2> createState() => _Lab9_2State();
}

class _Lab9_2State extends State<Lab9_2> {

  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome() async {
    await Future.delayed(Duration(seconds: 3), () {});
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)
    =>
        MyHomePage(title: "Kapil",)
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Text(
            "splash screen ",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 50,
              color: Colors.cyan,
            ),
          ),

        ),
      ),
    );
  }
}
