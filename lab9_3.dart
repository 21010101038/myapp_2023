import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab9_3 extends StatelessWidget {
  const Lab9_3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset('images/bg_matrimony_prelogin.jpg',fit: BoxFit.cover,),
                Container(
                  color: Color(0x99AAAAAAA),
                ),
                Column(
                  children: [
                    Container(child: Image.asset('images/bhart_metrimony_logo.png'),margin: EdgeInsets.only(top: 40),width: 180,height: 80),
                    Container(margin: EdgeInsets.only(top: 2,),child: Center(child: Text("INDIA\'S\nMOST TRUSTED\nMATRIMONY BRAND",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)),),
                  ],
                ),
              ],
            ),
                              flex: 14,
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                    child: Container(
                    child: Center(child: Text("Sign in",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black),)),
                      color: Colors.green,
                )),
                Expanded(
                    child: Container(
                      child: Center(child: Text("Sign Up",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black),)),
                      color: Colors.blueGrey,
                    )),

              ],
            ),
          ),
        ],
      ),
    );
  }
}
