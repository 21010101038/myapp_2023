import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'lab9_4_2.dart';

class Lab9_4_1 extends StatefulWidget {
  const Lab9_4_1({Key? key}) : super(key: key);

  @override
  State<Lab9_4_1> createState() => _Lab9_4_1State();
}

class _Lab9_4_1State extends State<Lab9_4_1> {
  var formkey = GlobalKey<FormState>();

  var UserNameController = TextEditingController();
  var UserWisisController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          "BirthdayDetail",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        )),
      ),
      body: SafeArea(
        minimum: EdgeInsets.only(top: 20),
        child: Center(
          child: Form(
            key: formkey,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        controller: UserNameController,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Password';
                          }

                          if (value.length < 8) {
                            return 'Enter Valid password';
                          }
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Name",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[100],
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextFormField(
                        controller: UserWisisController,
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Enter Wisis';
                          }

                          if (value.length < 8) {
                            return 'Enter Valid Wisis';
                          }
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Wisis",
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: InkWell(
                    onTap: () {
                      if (formkey.currentState!.validate()) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return Lab9_4_2(
                            name: UserNameController.text,
                            wisis: UserWisisController.text,
                          );
                        }));
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
