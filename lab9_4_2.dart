import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Lab9_4_2 extends StatelessWidget {
  var name,wisis;
  Lab9_4_2({this.name,this.wisis});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset('images/birdaycard.jpg',
                fit: BoxFit.cover,),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(name,style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Roboto-Italic.ttf",
                  ),
                    textAlign:TextAlign.center,),
                  Text(wisis,style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Roboto-Italic.ttf",
                  ),
                    textAlign:TextAlign.center,),
                ],
              ),
            ],
          ))
        ],
      ) ,
    );
  }
}
