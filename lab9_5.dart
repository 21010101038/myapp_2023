import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
int i=1;
int j=3;
class Lab9_5 extends StatefulWidget {
  const Lab9_5({Key? key}) : super(key: key);

  @override
  State<Lab9_5> createState() => _Lab9_5State();
}

class _Lab9_5State extends State<Lab9_5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Colors.tealAccent,
      body:Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child:InkWell(
                onTap: (){
                  getRandom();
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: Image.asset("images/dice$i.png"),
                ),
              ),

            ),
            Expanded(
              child:InkWell(
                onTap: (){
                  getRandom();
                },
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: Image.asset("images/dice$j.png"),
                ),
              ),

            ),
          ],
        ),
      ),
    );
  }void getRandom(){
    setState((){
      i=Random().nextInt(5)+1;
      j=Random().nextInt(5)+1;

    });
  }

}
